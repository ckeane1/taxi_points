import os
import pandas as pd
import numpy as np
from plotly import graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px


# POSITIONAL LIMITS
GPS_ABSOLUTE_MIN = 0 # feet || RESET IN THE FUNCTION BASED ON MINIMUM ALTITUDE

GPS_OBSTACLE_CLEARANCE_ALTITUDE = 50 + GPS_ABSOLUTE_MIN # feet

# VELOCITY LIMITS
AIRSPEED_TAXI_MAX = 59 # knots
AIRSPEED_TAXI_MID = 0 # knots
AIRSPEED_TAXI_MIN = 0 # knots

CLIMB_RATE_MIN = 1.016 # FPM

test_set_config_dictionary = {
    "20210611_whole" : {'mass': "4500"},
    "20210625_whole" : {'mass':  "4500"},
    "20210630_whole" : {'mass':  "4500"},
    "20210701_whole" : {'mass': "4500"},
    "20210707_whole" : {'mass':  "5110"},
    "20210715_whole" : {'mass':  "5500"},
    "20210720_whole" : {'mass':  "4500"}, 
    "20210722_whole" : {'mass':  "4500"},
}

def import_resample_and_label_time(path, usecols, resample_width):

    df = pd.read_csv(path, usecols=usecols)

    ## Convert timestrings under column 'Timestamp' to datetime64 under 'Datetime' and seconds elapsed under 'Elapsed'
    df['Datetime'] = pd.to_datetime(df['Timestamp'], format='%H:%M:%S:%f')
    df.set_index('Datetime', inplace=True)

    ## Resample data to a 1s period using the (mean) of each period
    period_string = str(resample_width) + 's'
    df = df.resample(period_string).mean()
    df['Elapsed'] = (df.index - pd.Timestamp("1970-01-01")) // pd.Timedelta('1ns')
    df['Elapsed'] = (df['Elapsed'] - df['Elapsed'].iloc[0]) / 10**9

    return ( df )

def acceleration_points(path, set) -> pd.DataFrame:
    """Calculate Ground Roll Distance and times for specific Pattern points.
    Args:
        pattern_points (pd.DataFrame): dataframe flight points below pattern
    Returns:
        pd.DataFrame: dataframe of just L/D points with L/D as a column
    """
    index = ['Timestamp']

    ## Pull in only the necessary and relevant variables from the CSVs to save on compute time

    velocities = ['True Airspeed', 'Airspeed', 'Ground Speed', 'Climb Rate', 'Angle of Attack']
    positions = ['GPS Altitude']
    angles = ['ECU Roll Angle', 'ECU Pitch Angle', 'ECU Yaw Angle']
    latlong = ['Latitude', 'Longitude']
    epu = ['Pusher EPU A Commanded Torque' , 'Pusher EPU B Commanded Torque']

    ## Define sets of the variables depending on where they are going

    usecols = index + velocities + positions + angles + latlong + epu
    differentiables = velocities + positions + angles

    ## Define all usable dataframes:

    storage_columns = ["Date","Weight","Roll Distance of Point","Roll Time of Point"]
    acceleration_data = pd.DataFrame(columns = storage_columns)
    deceleration_data = pd.DataFrame(columns = storage_columns)
    ## Read in CSV to DataFrame

    RESAMPLE_WIDTH = 1  ## INTEGER SECONDS
    test_points = import_resample_and_label_time(path, usecols, RESAMPLE_WIDTH)

    ## Primary filter to remove discontinuities like zeros or NaNs from the dataframe

    GPS_ABSOLUTE_MIN = test_points['GPS Altitude'].min() 
    gps_cutoff = test_points['GPS Altitude'] > GPS_ABSOLUTE_MIN
    test_points = test_points[(gps_cutoff)]

    ## Secondary filter to cut the data frame to just sections below takeoff speed but above taxi speed

    GPS_AIRSTRIP_ALTITUDE = test_points['GPS Altitude'].min()  ## Set new minimum to be the airstrip altitude
    GPS_GROUNDROLL_MAX = GPS_AIRSTRIP_ALTITUDE + 20
    GPS_GROUNDROLL_MIN = GPS_AIRSTRIP_ALTITUDE - 20

    ## TRANSLATE INTO FILTER ARGUMENTS

    gps_upper_groundroll = test_points['GPS Altitude'] < GPS_GROUNDROLL_MAX
    gps_lower_groundroll = test_points['GPS Altitude'] > GPS_GROUNDROLL_MIN 

    taxi_airspeed_upper = test_points['True Airspeed'] < AIRSPEED_TAXI_MAX
    taxi_airspeed_lower = test_points['True Airspeed'] > AIRSPEED_TAXI_MID 
    taxi_airspeed_min = test_points['True Airspeed'] > AIRSPEED_TAXI_MIN 

    ## APPLY FILTER
    acceleration_points_0_to_60 = test_points[ (taxi_airspeed_upper) & (taxi_airspeed_lower) ]
    acceleration_points_35_to_60 = acceleration_points[  (taxi_airspeed_min) ]
    
    ## USE TIME DIFFERENCING TO DIFFERENTIATE DISTINCT SECTIONS OF FILTERED DATA
    acceleration_points_0_to_60 = acceleration_points_0_to_60.reset_index()
    acceleration_points_0_to_60['Time Difference'] = acceleration_points_0_to_60['Elapsed'].diff()

    acceleration_points_35_to_60 = acceleration_points_0_to_60.reset_index()
    acceleration_points_35_to_60['Time Difference'] = acceleration_points_35_to_60['Elapsed'].diff()
    
    ## CONSTANTS AND LISTS

    ground_speed_list = []
    seconds_elapsed_list = []
    air_density_list = []

    TIME_DIFFERENCE_MIN = 10

    test_total_time = acceleration_points_0_to_60['Elapsed'].max()
    print("Total Time", test_total_time)

    WEIGHT = test_set_config_dictionary[set]['mass']

    for index, row in acceleration_points_35_to_60.iterrows():
        ## RECORD ALL THE DATA IN ONE DISCREET SECTION

        if row['Time Difference'] <= TIME_DIFFERENCE_MIN:

            ground_speed_list.append(row['Ground Speed']*3.2808399)
            seconds_elapsed_list.append(row['Elapsed'])
            air_density_list.append( 1.225 / ((row['True Airspeed'] / row["Airspeed"])**2))

        ## IF A TIME DIFFERENCE BIG ENOUGH IS FOUND, STOP RECORDING ... PERFORM A CALCULATION
        if (len(seconds_elapsed_list) > 10) and (row['Time Difference'] > TIME_DIFFERENCE_MIN) :

            ## TEST THAT THE CHANGE IN AIRSPEED OVER THE POINT IS LONG ENOUGH
            if ((row['True Airspeed'][-1] - row['True Airspeed'][0]) >  40):

                point_start_time = seconds_elapsed_list[0]
                point_end_time = seconds_elapsed_list[-1]
                point_time = (point_end_time - point_start_time)

                point_distance = np.trapz(ground_speed_list)

                average_density = ( 1.225 / ((row['True Airspeed'] / row["Airspeed"])**2) )
                average_density_altitude = ((-31204*np.average(air_density_list))+38074)
                print("ACCELERATION TO 60 - Distance", point_distance,"Time", point_time, "Elapsed start", seconds_elapsed_list[0] ,  "Elapsed start", seconds_elapsed_list[-1])

                acceleration_data["Test"] = [set]
                acceleration_data["Weight"] = [WEIGHT]
                acceleration_data["Roll Distance of Point"] = [point_distance]
                acceleration_data["Roll Time of Point"] = [point_time]
                acceleration_data["Average Air Density of Point"] = [average_density]
                acceleration_data["Average Density Altitude"] = [average_density_altitude]

            ## TEST THAT THE CHANGE IN AIRSPEED OVER THE POINT IS LONG ENOUGH
            if ((row['True Airspeed'][-1] - row['True Airspeed'][0]) <  40):

                point_start_time = seconds_elapsed_list[0]
                point_end_time = seconds_elapsed_list[-1]
                point_time = (point_end_time - point_start_time)

                point_distance = np.trapz(ground_speed_list)

                average_density = ( 1.225 / ((row['True Airspeed'] / row["Airspeed"])**2) )
                average_density_altitude = ((-31204*np.average(air_density_list))+38074)
                print("DECELERATION FROM 60 - Distance", point_distance,"Time", point_time, "Elapsed start", seconds_elapsed_list[0] ,  "Elapsed start", seconds_elapsed_list[-1])

                deceleration_data["Test"] = [set]
                deceleration_data["Weight"] = [WEIGHT]
                deceleration_data["Roll Distance of Point"] = [point_distance]
                deceleration_data["Roll Time of Point"] = [point_time]
                deceleration_data["Average Air Density of Point"] = [average_density]
                deceleration_data["Average Density Altitude"] = [average_density_altitude]

            ## RESET POINT LISTS      
            ground_speed_list = [] 
            seconds_elapsed_list = []
            air_density_list = []

    return (acceleration_points , acceleration_data , deceleration_data)


############
## MAIN
###########

## ADD A SERIES DATA PARSING FUNCTIONS (above) AND FEED ALIA 250 CSV FORMAT FLIGHT TEST DATA INTO IT

csv_folder_path = os.path.dirname(os.path.realpath(__file__)) + "/usable_csvs"

# List of files in the csv directory

csv_file_name_list = os.listdir(csv_folder_path)

print( "Found " + str(len(csv_file_name_list)) + " tests")

nameout = "takeoff_and_landing_data.csv"

set_list = []
acceleration_data_list = []
deceleration_data_list = []

fig7 = make_subplots(rows=1, cols=1, x_title = "Climbrate vs Torque Setting 30% - 100 %")
fig8 = make_subplots(rows=1, cols=1, x_title = "Climbrate vs Density Altitude 30% - 100 %")

altitude = np.arange(500 , 7000, 250 )
torque = np.arange(0.3 , 1 , 0.1 )
for set in csv_file_name_list:

    print("Reading in", set, "..." )


    # obstacle_clearance_points_output = obstacle_clearance_points(pattern_points_output[0])
    acceleration_points_output = acceleration_points(csv_folder_path + "/" +set,set)

    acceleration_data_list.append(acceleration_points_output[1])
    deceleration_data_list.append(acceleration_points_output[2])

acceleration_data = pd.concat(acceleration_data_list)    
deceleration_data = pd.concat(deceleration_data_list)    


# fig7.add_trace(go.Scatter(x=takeoff_roll_data["Date"][:8], y=takeoff_roll_data["Roll Distance of Point"],mode='markers'), row=1, col=1)

# fig8.add_trace(go.Scatter(x=landing_roll_data["Date"][:8], y=landing_roll_data["Roll Distance of Point"], mode='markers'), row=1, col=1)

# fig7.show()
# fig8.show()

# fig6 = make_subplots(rows=1, cols=1)
# fig6.add_trace(go.Scatter(x=low_torque_climb_rates_total["Average Air Density of Point"], y=low_torque_climb_rates_total["Average Climb Rate of Point"], mode='markers', name='Low Torque Climbs', color = low_torque_climb_rates_total["Date"]), row=1, col=1)
# fig6.add_trace(go.Scatter(x=high_torque_climb_rates_total["Average Air Density of Point"], y=high_torque_climb_rates_total["Average Climb Rate of Point"], mode='markers', name='High Torque Climbs',color =  high_torque_climb_rates_total["Date"]), row=1, col=1)
# fig6.show()

output_dataframe = pd.DataFrame()


set_list.append(set)