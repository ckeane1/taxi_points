# taxi_points

For finding the Time, Distance, Average Acceleration of taxi contained within an ALIA 250 style .csv.

## Project Purpose:
Sorting python based functions for flight data analysis. 

These functions intake the ALIA 250 CSV format and output desired plots and csv files. 

## Installation:
Make sure to have python 3.9 and a terminal downloaded.

Git Bash: A great terminal for interfacing with python.
https://git-scm.com/downloads

git clone git@gitlab.com:ckeane1/taxi_points.git

Create a folder in the root directory of this file called "usable_csvs".
(use gen_csv.py if applying this script to a segemented test set...)

python -m pip install --upgrade pip 
(pip is a python library installer that makes the following possible...)

pip install os
pip install pandas 
pip install numpy
pip install plotly.express

## Change directory to the root of the python file.

(list the current directory and its contents)
pwd

(back out a file layer with )
cd ../

(enter a file layer)
cd root_directory.py

## Run with:

"python taxi_points.py"
